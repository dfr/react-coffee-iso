var webpack = require('webpack');
require('es6-promise').polyfill();
var ExtractTextPlugin = require("extract-text-webpack-plugin");
module.exports = {  
  entry: ['./src/client/boot.cjsx', './css/main.less'],
  output: {
    path: './public',
    filename: 'bundle.js'
  },
  devtool: 'source-map',
  resolve: {
    extensions: ['', '.webpack.js', '.web.js', '.coffee', '.cjsx', '.js']
  },
  plugins: [
    //new webpack.optimize.UglifyJsPlugin()
      new ExtractTextPlugin("bundle.css")
  ],
  module: {
    loaders: [
      { test: /\.cjsx$/, loader: "coffee-jsx-loader?map" },
      { test: /\.coffee$/, loader: "coffee-jsx-loader?map" },
      { test: /\.less$/,
        loader: ExtractTextPlugin.extract("style-loader", "css-loader!less-loader")
      }
    ]
  }
}
