React = require('react');
update = require('react-addons-update');

class App extends React.Component

    constructor: (props) ->
        super props
        @state = 
            newItem:
                description: ''
            todoList: []

    changeName: (evt) =>
        v = evt.target.value
        newState = update @state, { newItem: { description: { $set: v } } }
        @setState newState
        true


    render: ->
        todoItems = ''
        <div>
            <div>
                <input type="text" placeholder="input new item" value={this.state.newItem.description} onChange={this.changeName} />
                <button onClick={this.addItem} >add</button>
            </div>
            <h4>{this.state.newItem.description}</h4>
            <ul>
            {<li key={n}>привет {n}</li> for n in [1..5]}
            </ul>
        </div>

module.exports = App
